import picamera
import time
import urllib
from http.server import BaseHTTPRequestHandler, HTTPServer

hostPort = 8081

def takePicture():
    print("capturing pic")
    camera = picamera.PiCamera()
    try:
        camera.capture('image.png')
    finally:
        camera.close()


def takeVideo():
    print("capturing video")
    camera = picamera.PiCamera()
    try:
        camera.start_recording('video.h264')
        camera.wait_recording(30)
        camera.stop_recording()
    finally:
        camera.close()


class MyServer(BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == "/pic":
            takePicture()

            self.send_response(200)
            self.send_header("Content-type", "image/png")
            self.end_headers()

            f = open('image.png', 'rb')
            self.wfile.write(f.read())
            f.close()
        elif self.path == "/video":
            takeVideo()

            self.send_response(200)
            self.send_header("Content-type", "video/H264")
            self.end_headers()

            f = open('video.h264', 'rb')
            self.wfile.write(f.read())
            f.close()
        else:
            self.send_response(404)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(bytes("<html><head><title>Not found</title></head>", "utf-8"))
            self.wfile.write(bytes("<body><p>Not found.</p>", "utf-8"))
            self.wfile.write(bytes("</body></html>", "utf-8"))

myServer = HTTPServer(("", hostPort), MyServer)
print(time.asctime(), "Server Starts - %s" % hostPort)

try:
    myServer.serve_forever()
except KeyboardInterrupt:
    pass

myServer.server_close()
print(time.asctime(), "Server Stops - %s" % hostPort)
